# Birds Drupal Site

###Golocating bird sightings

Exposes drupal entities through a REST API using the [RESTful Web Services](http://drupal.org/project/restws) module.

SQL DB is available in the the [Downloads](https://bitbucket.org/chandima/birds/downloads) area.

####Admin credentials:
	username = admin
	password = admin